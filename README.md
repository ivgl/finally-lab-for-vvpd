# Лаборатораная №5
Что было проделанно в этой работе? Вот **подробное описание** моей лабы:

* написана документация к функциям 
* создан HTML файл через Doxypy
* работа в Gitlab c README

**_Что пошло не так при выполнении задания?_**
1. Doxygen не предназначен для питона
2. Разные версии Doxygen по разному создаю HTML файлы

~~Вот фулл моего кода~~
```
#проверка ввода
def check_input(A,B):
    try:
        A = int(A)
        B = int(B)
        if A > B:
            return False
    except ValueError:
        return False
    return True

#основной алгоритм
# Translte in 2 sys
lst = []
def prog(A,B):

    A = int(A)
    B = int(B)
    for i in range(A,B+1):
                target = bin(i)[2:]
                lst.append(target)
    #make value str -> int
    for i, item in enumerate(lst):
        lst[i] = int(item)
    print(lst)
    #check zero
    q_range = len(lst)
    sort_lst = []
    for i in range(q_range):
        q = 0
        while lst[i] != 0:
            if lst[i] % 10 == 0:
                q = q + 1
            lst[i] = lst[i] // 10
        sort_lst.append(q)
    print(sort_lst)
    #find in list one zero
    maxx = 0
    for i in range(len(sort_lst)):
        if sort_lst[i] == 1:
            maxx = maxx + 1
    return maxx

#ввод аргументов
def main():
    A =  input("Введите 1 число: ",)
    B =  input("Введите 2 число:",)
    if check_input(A, B):
        A = int(A)
        B = int(B)
        print(prog(A,B))
    else:
        print("Wrong")

if __name__ == "__main__":
    main()
    
#test's for func check_input
def test_wrongdig():

    assert check_input('a',5) == False
    assert check_input(6, 'a') == False
    assert check_input('g','h') == False
    assert check_input(5, 2) == False

#test's for func prog    
def test_1():
    assert prog(5,10) == 2
    assert prog(5,10) == 2
#Ban
```
[Добавте меня в друзья на е курсах](https://e.sfu-kras.ru/login/index.php?testsession=140124)
![alt Бан](https://store-images.s-microsoft.com/image/apps.56074.13510798887869659.a3d47ea0-c60f-40cf-9b27-aaf86105ab0f.c54986ae-275f-4100-bfed-35bda676557a?mode=scale&q=90&h=1080&w=1920)