#проверка ввода
def check_input(A,B):
    """
    Args:
        args1: int dig
        arg2: int dig
    """
    try:
        A = int(A)
        B = int(B)
        if A > B:
            return False
    except ValueError:
        return False
    return True

#основной алгоритм
# Translte in 2 sys
lst = []
def prog(A,B):
    """Docymentation

    Переводит числа в промежутке  в двоичную систему счисления
    и ищет число с одним нулем.

    Args:
        arg1: начало промежутка.
        arg2: конец промежутка.

    Returns:
        maxx, это резулятат вычислений 
    
    Raises:
        ValueError, TyoeError
    
   
    Examples:
        Пример работы программы:
        На вход подаются числа от 5 до 10. Числа 5,6,7,8,9,10 переведутся в
        двоичную систему счисления.После этого все числа проверятся на 1 ноль.
        Результатом работы программы будет 2.
        
        |Number one
        |Введите 1 число: 10
        |Введите 2 число:20
        |3

        |Number two
        |Введите 1 число: a
        |Введите 2 число:4
        |Wrong

        |Number three
        |Введите 1 число: 10
        |Введите 2 число:2
        |Wrong

    """
    A = int(A)
    B = int(B)
    for i in range(A,B+1):
                target = bin(i)[2:]
                lst.append(target)
    #make value str -> int
    for i, item in enumerate(lst):
        lst[i] = int(item)
    print(lst)
    #check zero
    q_range = len(lst)
    sort_lst = []
    for i in range(q_range):
        q = 0
        while lst[i] != 0:
            if lst[i] % 10 == 0:
                q = q + 1
            lst[i] = lst[i] // 10
        sort_lst.append(q)
    print(sort_lst)
    #find in list one zero
    maxx = 0
    for i in range(len(sort_lst)):
        if sort_lst[i] == 1:
            maxx = maxx + 1
    return maxx

#ввод аргументов
def main():
    """Docymentashion

    A: начало промежутка.
    B: конец промежутка.
    Если ввести AAA и BBB, выведится Wrong, Wrong
    говорит нам о том, что значения введены
    не корректно
    
    """
    A =  input("Введите 1 число: ",)
    B =  input("Введите 2 число:",)
    if check_input(A, B):
        A = int(A)
        B = int(B)
        print(prog(A,B))
    else:
        print("Wrong")

if __name__ == "__main__":
    main()
    
#test's for func check_input
def test_wrongdig():
    """Docymentation

    1) 1 не корректное значение
    2) 2 не коррекктоне значение
    3) оба не корректные значения
    4) не правильный промежуток
    
    """
    assert check_input('a',5) == False
    assert check_input(6, 'a') == False
    assert check_input('g','h') == False
    assert check_input(5, 2) == False

#test's for func prog    
def test_1():
    """Docymentation

    Правильные значения 
    """
    assert prog(5,10) == 2
    assert prog(5,10) == 2
#Ban
